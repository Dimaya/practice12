"use strict";
let numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '12');
while (numberOfFilms == null) {
    numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '');
}
while (numberOfFilms == 0) {
    numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '');
}

let movie = prompt('Один из последних просмотренных фильмов?', 'logan');
while (movie == null) {
    movie = prompt('Один из последних просмотренных фильмов?', '');
}
while (movie.length >= 50) {
    movie = prompt('Один из последних просмотренных фильмов?', '');
};
const rating = prompt('На сколько оцените его?', '8.1')


const personalMovieDB = {
    count: (`${numberOfFilms}`),
    movies: { [movie]: rating, },
    actors: {},
    genres: [],
    privat: false,
};


if (personalMovieDB.count <= 10) {
    alert('Просмотрено довольно мало фильмов');
} else if (personalMovieDB.count <= 30) {
    alert('Вы классический зритель');
} else if (personalMovieDB.count > 30) {
    alert('Вы киноман');
} else {
    alert('Произошла ошибка');
}

//пусть и не обязательно, но я объявлю)

function writeYourGenres() { //мы же не задаем никакой начальный аргумент для функции?// 
    personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
    personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
    personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
}
writeYourGenres();
function showMyDB() {
    if (personalMovieDB.privat == false) {
        console.log(personalMovieDB);
    };
};
showMyDB();

// Тут есть пересекающиеся переменные. Во избежании ошибок я всё решил закомментировать. Спасибо.
// ps. Надесюь их не надо было менять <(-_-)>

// let num = 20;
// function showFirstMassage(text){
//     console.log(text);
//      num = 10;
// }
// showFirstMassage('hello world');
// console.log(num);

// function calc(a, b) {
//     return(a + b);
// }

// console.log(calc(4,3));
// console.log(calc(5,6));
// console.log(calc(10,6));

// function ret() {
//     let num = 50;
//     return(num);
// }

// const anotherNum = ret();
// console.log(anotherNum);

// const logger = function() {

// };

// const str = 'text';
// const arr = [1, 2, 3];

// console.log(str.toUpperCase())
// console.log(str.toLowerCase())

// const fruit = 'Some Fruit';
// console.log(fruit.indexOf("Fruit"));

// const logg = 'Hello world';

// console.log(logg.slice(6, 11));
// console.log(logg.substring(6, 11));
// console.log(logg.substr(6, 3));

// const num = 12.2;
// console.log(Math.round(num));

// const test = '12.2px';
// console.log(parseInt(test));
// console.log(parseFloat(test));